'use strict';

const mongoose = require('mongoose'),
    schema = mongoose.Schema;

let storiesHeadingSchema = new schema({
    "HeadingText" : { type: String, required: true},
    "ImgVideolink" : { type: String, required: true},
    "FeedOrder" : { type: String, required: true},
    "Visible" : { type: Boolean, required: true},
    "FeedType" : { type: Number, required: true},
    "IsSpecial" :  { type: Boolean, required: true},
    "IsDeleted" :  { type: Boolean, required: true},
    "CreatedDate" : { type: Date, required: true, default: Date.now},
    "ModifiedDate" : { type: Date },
    "CreatedBy" : { type: String, required: true}
});

module.exports.storiesHeading = mongoose.model('storiesheading', storiesHeadingSchema);
