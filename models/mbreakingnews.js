'use strict';

const mongoose = require('mongoose'),
    schema = mongoose.Schema;

let breakingnewssliderSchema = new schema({
    "HeadingText" : { type: String, required: true},
    "twitterURL" : { type: String, required: true},
    "FeedOrder" : { type: String, required: true},
    "Visible" : { type: Boolean, required: true},
    "IsDeleted" :  { type: Boolean, required: true},
    "CreatedDate" : { type: Date, required: true, default: Date.now},
    "CreatedBy" : { type: String, required: true},
    "UpdatedDate" : { type: Date,required: true, default: Date.now },
    "UpdatedBy" : { type: String, required: true}
});

module.exports.mbreakingnews = mongoose.model('breakingnewssliders', breakingnewssliderSchema);