const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    name:  {type: String, required: true}, // String is shorthand for {type: String}
    age: Number,
    isSingle:Boolean,
    CrAt: { type: Date, default: Date.now }
});

module.exports = {User:mongoose.model('users', userSchema)};