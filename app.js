const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

//db connection
mongoose.connect('mongodb://localhost:27017/valaiyosai', {useNewUrlParser: true, useUnifiedTopology: true});


const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const storyRouter = require('./routes/storiesheading');
const breakingNewsRouter = require('./routes/mbreakingnews');

const app = express();

app.use(bodyParser.json());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use( storyRouter);
app.use( breakingNewsRouter);

module.exports = app;
