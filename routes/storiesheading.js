const express = require('express');
const router = express.Router();
const mStoriesHeading = require('../models/storiesheading').storiesHeading;

/* GET users listing. */
router.get('/getHeading', function(req, res, next) {
  mStoriesHeading.find({},{},{lean:true},(err,docs)=>{
    if(err)
      res.status(500).json(err);
    else
      res.status(200).json({statusCode:200, data:docs});
  });
});

router.post('/createHeading', function(req, res, next) {
  console.log('POST')
  let newStoriesHeading = new mStoriesHeading();

  newStoriesHeading.HeadingText = req.body.HeadingText;
  newStoriesHeading.ImgVideolink = req.body.ImgVideolink; 
  newStoriesHeading.FeedOrder = req.body.FeedOrder; 
  newStoriesHeading.Visible = req.body.Visible; 
  newStoriesHeading.FeedType = req.body.FeedType;
  newStoriesHeading.IsSpecial = req.body.IsSpecial;
  newStoriesHeading.IsDeleted = req.body.IsDeleted;
  newStoriesHeading.CreatedBy = req.body.CreatedBy;

  newStoriesHeading.save((err)=>{
    console.log(err);
    if(err)
      res.status(500).json(err);
    else
      res.status(200).json({statusCode:200, message:"Success"});
  });
});

router.post('/updateHeading', function(req, res, next) {
  console.log('POST');
  console.log(typeof res);
  var newvalues = {$set: {HeadingText: req.body.HeadingTex,ImgVideolink: req.body.ImgVideolink,FeedOrder: req.body.FeedOrder,Visible :req.body.Visible,FeedType:req.body.FeedType,IsSpecial:req.body.IsSpecial,IsDeleted:req.body.IsDeleted,CreatedBy: req.body.CreatedBy} };
  console.log(typeof res.status)

  mStoriesHeading.updateOne({_id: req.body.id}, newvalues, function(err, doc) {
    if(err)
      res.status(500).json(err);
    else
      res.status(200).json({statusCode:200, message:"Success"});
  });

});


router.delete('/deleteHeading', function(req, res, next) {
  var myquery =  {_id: req.body.id};
  mStoriesHeading.deleteOne(myquery, function(err, obj) {
    if(err)
      res.status(500).json(err);
    else
      res.status(200).json({statusCode:200, message:"Success"});
  });
});






module.exports = router;
