const express = require('express');
const router = express.Router();
const breakingnews = require('../models/mbreakingnews').mbreakingnews;

/* GET users listing. */
router.get('/getBreakingNews', function(req, res, next) {
    breakingnews.find({},{},{lean:true},(err,docs)=>{
        console.log(docs);
        console.log(err);
    if(err)
      res.status(500).json(err);
    else
      res.status(200).json({statusCode:2000, data:docs});
  });
});

router.post('/createBreakingNews', function(req, res, next) {
    let newmbreakingnews = new breakingnews();
    newmbreakingnews.HeadingText = req.body.HeadingText;
    newmbreakingnews.twitterURL = req.body.twitterURL; 
    newmbreakingnews.FeedOrder = req.body.FeedOrder; 
    newmbreakingnews.Visible = req.body.Visible;
    newmbreakingnews.IsDeleted = req.body.IsDeleted;
    newmbreakingnews.CreatedBy = req.body.CreatedBy;
    newmbreakingnews.UpdatedBy = req.body.UpdatedBy;
  
    newmbreakingnews.save((err)=>{
      console.log(err);
      if(err)
        res.status(500).json(err);
      else
        res.status(200).json({statusCode:200, message:"Success"});
    });
  });


module.exports = router;
