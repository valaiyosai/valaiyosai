const express = require('express');
const router = express.Router();
const mUser = require('../models/muser').User;

/* GET users listing. */
router.get('/', function(req, res, next) {
  mUser.find({},{},{lean:true},(err,docs)=>{
    if(err)
      res.status(500).json(err);
    else
      res.status(200).json({statusCode:200, data:docs});
  });
});

router.post('/', function(req, res, next) {
  
  newUser = new mUser();
  newUser.name = req.body.userName;
  newUser.age = req.body.userAge;
  newUser.isSingle = req.body.userStatus;

  newUser.save((err)=>{
    console.log(err);
    if(err)
      res.status(500).json(err);
    else
      res.status(200).json({statusCode:200, message:"Success"});
  });
});



module.exports = router;
